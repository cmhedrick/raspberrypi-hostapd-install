# raspberrypi-hostapd-install

A single script to config hostapd on the raspberry pi to use wlan0 (or other interface) as an access point. Finally putting an end to having to type out all the BS from a [blog](https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/)(not exactly that one but close enough may the OG RIP...) from a year ago who couldn't be bothered to JUST MAKE THE SCRIPT ORIGINALLY JFC.

## Pre-Req:
1. Make sure to have configured your Pi to have SSH on
2. Configure the wpa_supplicant.conf file for your wifi so you can connect to your pi initially
3. Plug in a WiFi Adapdeter, 2 if your Pi doens't have a built-in WiFi module
4. SSH into the Pi and clone this repo onto the Pi or SCP this project to the Pi

## Running the Script
1. once the files are on the pi go to the root of the project directory and run `chmod +x hostapd-install.sh`
2. configure the files. (if you don't know what you're doing just change the `ssid` and `wpa_passphrase` fields of the `hostapd.conf`)
3. run the script (**requires sudo**): `./hostapd-install.sh`

## Contributing
Feel free to send an issue, or enhancement request. PR's welcome!
Even fork the repo, just don't be a dick.

## Authors and acknowledgment
- Me (@cmhedrick), yes I'm super cool and super smart.

## License
One of the best: https://dbad-license.org/

[See license in repo.](https://gitlab.com/cmhedrick/raspberrypi-hostapd-install/-/blob/main/LICENSE)

## Project status
Currently being maintained as of 11/2021

## Road map/TODO
- Make script simple interactive mode: prompts for SSID and Passphrase
- Make script adv. interactive mode: prompts for the above but also IP range and interfaces.