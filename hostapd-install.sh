# install packages
sudo apt install dnsmasq hostapd -y

# stop the services
sudo systemctl stop dnsmasq
sudo systemctl stop hostapd

sudo rm /etc/dhcpcd.conf

# copy over dhcpcd
sudo cp ./dhcpcd.conf /etc/dhcpcd.conf

sudo service dhcpcd restart

#set up dhcp server
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
sudo cp ./dnsmasq.conf /etc/dnsmasq.conf
sudo systemctl start dnsmasq

sudo rm /etc/default/hostapd
sudo cp .hostapd /etc/default/hostapd
sudo cp ./hostapd.conf /etc/hostapd/hostapd.conf

sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl start hostapd
